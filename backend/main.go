package main

import (
	"os"
	"os/signal"
	"syscall"

	routers "./routers"
	services "./services/api/v1"
)

func main() {
	// Detect Ctrl+C signal
	c := make(chan os.Signal)

	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		services.CloseDB(services.DB)
		os.Exit(1)
	}()

	routers.HandleRequest()
}
