package controllers

import (
	"encoding/json"
	"net/http"

	models "../../../models"
	services "../../../services/api/v1"
)

// GetDepartments return all the departments in the database
func GetDepartments(w http.ResponseWriter, r *http.Request) {
	results, err := services.DB.Query("CALL GetAllDepartments();")

	var departments []models.Department

	if err != nil {
		panic(err.Error())
	}

	for results.Next() {
		var department models.Department

		err = results.Scan(&department.IDDepartment, &department.Name)

		if err != nil {
			panic(err.Error())
		}

		departments = append(departments, department)
	}

	defer results.Close()

	json.NewEncoder(w).Encode(departments)
}
