package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	models "../../../models"
	services "../../../services/api/v1"
)

// Register is used to create a new user
func Register(w http.ResponseWriter, r *http.Request) {
	var person models.Person

	reqBody, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(reqBody, &person)

	results, err := services.DB.Exec("CALL CreateUser(?, ?, ?, ?, ?, ?, ?, ?, ?);",
		person.IDDepartment, person.IDRole, person.Name, person.LastName,
		person.Age, person.Email, person.CellphoneNumber, person.UserName,
		person.Password)

	if err != nil {
		panic(err.Error)
	}

	fmt.Println(results)
	fmt.Println(results.LastInsertId())
	json.NewEncoder(w).Encode(results)
}

// GetRoles returns all user roles
func GetRoles(w http.ResponseWriter, r *http.Request) {
	results, err := services.DB.Query("CALL GetAllRoles();")

	if err != nil {
		panic(err.Error())
	}

	var roles []models.Role

	for results.Next() {
		var role models.Role

		err := results.Scan(&role.IDRole, &role.Name)

		if err != nil {
			panic(err.Error())
		}

		roles = append(roles, role)
	}

	defer results.Close()

	json.NewEncoder(w).Encode(roles)
}

// GetAllStudents returns all students in database
func GetAllStudents(w http.ResponseWriter, r *http.Request) {
	results, err := services.DB.Query("CALL GetAllStudents();")

	if err != nil {
		panic(err.Error())
	}

	var students []models.PersonInfo

	for results.Next() {
		var student models.PersonInfo

		err := results.Scan(&student.IDPerson, &student.Name,
			&student.LastName, &student.Age, &student.Email,
			&student.CellphoneNumber, &student.UserName, &student.Department)

		if err != nil {
			panic(err.Error())
		}

		student.Role = "Student"
		students = append(students, student)
	}

	defer results.Close()

	json.NewEncoder(w).Encode(students)
}

// GetAllTeachers returns all teachers in database
func GetAllTeachers(w http.ResponseWriter, r *http.Request) {
	results, err := services.DB.Query("CALL GetAllTeachers();")

	if err != nil {
		panic(err.Error())
	}

	var teachers []models.PersonInfo

	for results.Next() {
		var teacher models.PersonInfo

		err := results.Scan(&teacher.IDPerson, &teacher.Name,
			&teacher.LastName, &teacher.Age, &teacher.Email,
			&teacher.CellphoneNumber, &teacher.UserName, &teacher.Department)

		if err != nil {
			panic(err.Error())
		}

		teacher.Role = "Teacher"
		teachers = append(teachers, teacher)
	}

	defer results.Close()

	json.NewEncoder(w).Encode(teachers)
}

// GetAllPeople returns all people in database
func GetAllPeople(w http.ResponseWriter, r *http.Request) {
	results, err := services.DB.Query("CALL GetAllPeople();")

	if err != nil {
		panic(err.Error())
	}

	var people []models.PersonInfo

	for results.Next() {
		var person models.PersonInfo

		err := results.Scan(&person.IDPerson, &person.Name, &person.LastName,
			&person.Age, &person.Email, &person.CellphoneNumber,
			&person.UserName, &person.Role, &person.Department)

		if err != nil {
			panic(err.Error())
		}

		people = append(people, person)
	}

	defer results.Close()

	json.NewEncoder(w).Encode(people)
}
