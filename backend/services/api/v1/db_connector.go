package services

import (
	"database/sql"
	"fmt"

	// Connect to the database
	_ "github.com/go-sql-driver/mysql"
)

// ConnectToDB is used to connect at MySQL database
func ConnectToDB() (db *sql.DB) {
	db, err := sql.Open("mysql", "root:password@tcp(127.0.0.1:3306)/college")

	// if there is an error opening the connection, handle it
	if err != nil {
		panic(err.Error())
	}

	fmt.Println("Connected to DB.")
	return
}

// CloseDB is used to close MySQL database
func CloseDB(db *sql.DB) {
	defer db.Close()
	fmt.Println("\nDB connection closed.")
}

var DB *sql.DB = ConnectToDB()
