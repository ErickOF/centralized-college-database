package models

// Department model
type Department struct {
	IDDepartment int    `json:"idDepartment"`
	Name         string `json:"name"`
}
