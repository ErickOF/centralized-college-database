package models

// Person model
type Person struct {
	IDDepartment    int
	IDRole          int
	Name            string
	LastName        string
	Age             int8
	Email           string
	CellphoneNumber int
	UserName        string
	Password        string
}

// PersonInfo model
type PersonInfo struct {
	IDPerson        int
	Name            string
	LastName        string
	Age             int8
	Email           string
	CellphoneNumber int
	UserName        string
	Department      string
	Role            string
}
