package models

// Role model
type Role struct {
	IDRole string `json:"idRole"`
	Name   string `json:"name"`
}
