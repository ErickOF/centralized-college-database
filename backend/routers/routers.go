package routers

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the Home Page!")
}

// HandleRequest is used to handle all the requests on the API
func HandleRequest() {
	router := mux.NewRouter().StrictSlash(false)
	router.HandleFunc("/", homePage).Methods("GET")

	handleDepartments(router)
	handleUsers(router)

	fmt.Println("Listening on http://localhost:10000/")
	log.Fatal(http.ListenAndServe(":10000", router))
}
