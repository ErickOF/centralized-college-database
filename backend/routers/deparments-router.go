package routers

import (
	controllers "../controllers/api/v1"
	"github.com/gorilla/mux"
)

func handleDepartments(router *mux.Router) {
	router.HandleFunc("/api/v1/departments/get-all", controllers.GetDepartments).Methods("GET")
}
