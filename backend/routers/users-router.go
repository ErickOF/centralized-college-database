package routers

import (
	controllers "../controllers/api/v1"
	"github.com/gorilla/mux"
)

func handleUsers(router *mux.Router) {
	router.HandleFunc("/api/v1/users/register", controllers.Register).Methods("POST")
	router.HandleFunc("/api/v1/users/get-roles", controllers.GetRoles).Methods("GET")
	router.HandleFunc("/api/v1/users/get-all-students", controllers.GetAllStudents).Methods("GET")
	router.HandleFunc("/api/v1/users/get-all-teachers", controllers.GetAllTeachers).Methods("GET")
	router.HandleFunc("/api/v1/users/get-all-people", controllers.GetAllPeople).Methods("GET")
}
