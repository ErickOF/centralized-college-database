DROP DATABASE college;

CREATE DATABASE college;

CREATE TABLE IF NOT EXISTS college.department(
    id_department INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY(id_department)
);

CREATE TABLE IF NOT EXISTS college.role(
    id_role INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(50),
    PRIMARY KEY(id_role)
);

CREATE TABLE IF NOT EXISTS college.period(
    id_period INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(50),
    PRIMARY KEY(id_period)
);

CREATE TABLE IF NOT EXISTS college.person(
    id_person INT AUTO_INCREMENT NOT NULL,
    id_department INT NOT NULL,
    id_role INT NOT NULL,
    name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    age TINYINT NOT NULL,
    email VARCHAR(100) NOT NULL,
    cellphone_number INT NOT NULL,
    user_name VARCHAR(20) NOT NULL,
    _password VARCHAR(256) NOT NULL,
    PRIMARY KEY(id_person),
    FOREIGN KEY(id_department) REFERENCES college.department(id_department),
    FOREIGN KEY(id_role) REFERENCES college.role(id_role)
);

CREATE TABLE IF NOT EXISTS college.cours(
    id_cours INT AUTO_INCREMENT NOT NULL,
    id_department INT NOT NULL,
    name VARCHAR(100) NOT NULL,
    code VARCHAR(8) NOT NULL,
    PRIMARY KEY(id_cours),
    FOREIGN KEY(id_department) REFERENCES college.department(id_department)
);

CREATE TABLE IF NOT EXISTS college.class(
    id_class INT AUTO_INCREMENT NOT NULL,
    id_cours INT NOT NULL,
    id_person INT NOT NULL,
    id_period INT NOT NULL,
    class_number TINYINT NOT NULL,
    capacity TINYINT NOT NULL,
    _year TINYINT NOT NULL,
    start_hour TIME NOT NULL,
    end_hour TIME NOT NULL,
    PRIMARY KEY(id_class),
    FOREIGN KEY(id_cours) REFERENCES college.cours(id_cours),
    FOREIGN KEY(id_person) REFERENCES college.person(id_person),
    FOREIGN KEY(id_period) REFERENCES college.period(id_period)
);

CREATE TABLE IF NOT EXISTS college.studentxclass(
    id_studentxclass INT AUTO_INCREMENT NOT NULL,
    id_person INT NOT NULL,
    id_class INT NOT NULL,
    PRIMARY KEY(id_studentxclass),
    FOREIGN KEY(id_person) REFERENCES college.person(id_person),
    FOREIGN KEY(id_class) REFERENCES college.class(id_class)
);
