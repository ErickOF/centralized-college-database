USE college;

DROP PROCEDURE IF EXISTS GetAllDepartments;

CREATE PROCEDURE GetAllDepartments()
BEGIN
    SELECT id_department, name
    FROM department;
END;
