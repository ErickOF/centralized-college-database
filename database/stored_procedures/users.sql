USE college;

-- Delete procedures
DROP PROCEDURE IF EXISTS GetAllRoles;
DROP PROCEDURE IF EXISTS CreateUser;
DROP PROCEDURE IF EXISTS GetAllStudents;
DROP PROCEDURE IF EXISTS GetAllTeachers;


-- Get all roles
CREATE PROCEDURE GetAllRoles()
BEGIN
    SELECT id_role, name
    FROM role;
END;


-- Create a new user
CREATE PROCEDURE CreateUser(
    IN pid_department INT,
    IN pid_role INT,
    IN pname VARCHAR(50),
    IN plast_name VARCHAR(50),
    IN page TINYINT,
    IN pemail VARCHAR(100),
    IN pcellphone_number INT,
    IN puser_name VARCHAR(20),
    IN ppassword VARCHAR(256)
)
BEGIN
    INSERT INTO person (id_department, id_role, name, last_name, age, email, cellphone_number, user_name, _password)
    VALUES (pid_department, pid_role, pname, plast_name, page, pemail, pcellphone_number, puser_name, ppassword);
END;


-- Get all students
CREATE PROCEDURE GetAllStudents()
BEGIN
    SELECT P.id_person, P.name, P.last_name, P.age, P.email, P.cellphone_number, P.user_name, D.name AS department
    FROM person AS P
    INNER JOIN role AS R
    ON P.id_role = R.id_role
    INNER JOIN department AS D
    ON P.id_department = D.id_department
    WHERE R.name = "Student";
END;


-- Get all teachers
CREATE PROCEDURE GetAllTeachers()
BEGIN
    SELECT P.id_person, P.name, P.last_name, P.age, P.email, P.cellphone_number, P.user_name, D.name AS department
    FROM person AS P
    INNER JOIN role AS R
    ON P.id_role = R.id_role
    INNER JOIN department AS D
    ON P.id_department = D.id_department
    WHERE R.name = "Teacher";
END;

-- Get all people
CREATE PROCEDURE GetAllPeople()
BEGIN
    SELECT P.id_person, P.name, P.last_name, P.age, P.email, P.cellphone_number, P.user_name, R.name AS role, D.name AS department
    FROM person AS P
    INNER JOIN role AS R
    ON P.id_role = R.id_role
    INNER JOIN department AS D
    ON P.id_department = D.id_department;
END;
