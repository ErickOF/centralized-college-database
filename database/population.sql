-- Departments
INSERT INTO college.department (name)
VALUES ("English");

INSERT INTO college.department (name)
VALUES ("Mathematics");

INSERT INTO college.department (name)
VALUES ("History");

INSERT INTO college.department (name)
VALUES ("Engineering");


-- Courses
INSERT INTO college.cours (id_department, name, code)
VALUES (1, "Level 1", "ENG11000");
INSERT INTO college.cours (id_department, name, code)
VALUES (1, "Level 2", "ENG12000");
INSERT INTO college.cours (id_department, name, code)
VALUES (1, "Level 3", "ENG21000");
INSERT INTO college.cours (id_department, name, code)
VALUES (1, "Level 4", "ENG22000");
INSERT INTO college.cours (id_department, name, code)
VALUES (1, "Level 5", "ENG31000");
INSERT INTO college.cours (id_department, name, code)
VALUES (1, "Level 6", "ENG32000");
INSERT INTO college.cours (id_department, name, code)
VALUES (1, "Level 7", "ENG41000");
INSERT INTO college.cours (id_department, name, code)
VALUES (1, "Level 8", "ENG42000");
INSERT INTO college.cours (id_department, name, code)
VALUES (1, "Level 9", "ENG51000");
INSERT INTO college.cours (id_department, name, code)
VALUES (1, "Level 10", "ENG52000");

INSERT INTO college.cours (id_department, name, code)
VALUES (2, "Pre Calculus", "MAT11000");
INSERT INTO college.cours (id_department, name, code)
VALUES (2, "One Variable Calculus", "MAT12000");
INSERT INTO college.cours (id_department, name, code)
VALUES (2, "Linear Algebra", "MAT21000");
INSERT INTO college.cours (id_department, name, code)
VALUES (2, "Multivariable Calculus", "MAT22000");
INSERT INTO college.cours (id_department, name, code)
VALUES (2, "Differential Equations", "MAT31000");

INSERT INTO college.cours (id_department, name, code)
VALUES (3, "World War I", "HIS11000");
INSERT INTO college.cours (id_department, name, code)
VALUES (3, "World War II", "HIS12000");
INSERT INTO college.cours (id_department, name, code)
VALUES (3, "Azteca", "HIS23000");

INSERT INTO college.cours (id_department, name, code)
VALUES (4, "Introduction to Circuits", "EGG11000");
INSERT INTO college.cours (id_department, name, code)
VALUES (4, "DC Circuits", "EGG12000");
INSERT INTO college.cours (id_department, name, code)
VALUES (4, "DC Circuits Laboratory", "EGG12001");
INSERT INTO college.cours (id_department, name, code)
VALUES (4, "AC Circuits Laboratory", "EGG22001");
INSERT INTO college.cours (id_department, name, code)
VALUES (4, "AC Circuits", "EGG22000");


-- Periods
INSERT INTO college.period (name)
VALUES ("Summer");
INSERT INTO college.period (name)
VALUES ("Semester 1");
INSERT INTO college.period (name)
VALUES ("Semester 2");

-- Roles
INSERT INTO college.role (name)
VALUES ("Student");
INSERT INTO college.role (name)
VALUES ("Teacher");
