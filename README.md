# Centralized-College-Database

Personal project to practice MySQL and learn Go and Elm.

A college has academic departments, such as the Department of English, Department of Mathematics, Department of History, and so on. And each department offers a variety of courses. Now, an instructor can teach more than one course. Let’s say a professor takes a class on Statistics and also on Calculus.

As a student in the Mathematics department, you can enroll in both of these courses. Therefore, every college course can have any number of students. Here, an important point to note is that a particular course can have only one instructor to avoid overlaps. 

Taken from: https://www.upgrad.com/blog/sql-project-ideas-topics-for-beginners/
